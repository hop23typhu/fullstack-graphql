# Basic writing and formating syntax of readme file

## headings

To create a heading, add one to six # symbols before your heading text. The number of # you use  will determine the size of the heading.

```
# the largest heading
## the second largest heading
###### the smallest heading
```

## styling text
1, bold: syntax ** ** 
`
** this is bold text**
`
2, italic: syntax * * 
*this is italic text*
3, strike-through
~~ this is mistaken text ~~
 
## quoting code
Example: some basic git commands are:
```
git status
git add
git commit
git push
```
## link 
You can create an inline link by wrapping link text in brackets [ ], and then wrapping the URL in parentheses ( ) You can also use the keyboard shortcut command + k to create a link
Example:
my name is [ luong ba hop ](http://teachyourself.vn)

## lists
You can make ann unordered list by preceding one or more lines of text with - or *

```
- Luong Ba Hop
- Nguyen Duc Thang
- Ha Van Tu
- Sac Ngoc Khang
```

```
* Luong Ba Hop
* Sac Ngoc Khang
* Pham Van An

```
To order your list, precede each line with a number
```
1. John 
2. Steve
3. Mark
```

```
1. List outstanding developers:
  - Peter
  - Mark
  - Hop
2. List outstanding projects:
 * Veritone
 * Frequency
 * Vault dragon
```

 ## image
 this is my image in 2018

 this is my image ![luong ba hop](https://i0.wp.com/teachyourself.vn/wp-content/uploads/2015/10/28168149_867975846717615_7950735090634773589_n.jpg?w=960)

